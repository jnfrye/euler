import sys
sys.path.append(sys.path[0] + "/src")  # also import from the subdirectory /src

import euler1
import euler2
import euler3
import euler4
import euler5
import euler6
import euler7
import euler8
import euler9
import euler10
import euler11


# print euler11.max_grid_product(4, "number_array.txt")


print euler11.max_2d_multidirectional_product()
